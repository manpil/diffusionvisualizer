﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Benchmark.cs" company="Manfred Pilhar">
//   Manfred Pilhar
// </copyright>
// <summary>
//   Defines the Benchmark type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DiffusionVisualizer
{
    using System;

    /// <summary>
    /// The benchmark.
    /// </summary>
    public class Benchmark
    {
        /// <summary>
        /// The start date.
        /// </summary>
        private static DateTime startDate = DateTime.MinValue;

        /// <summary>
        /// The end date.
        /// </summary>
        private static DateTime endDate = DateTime.MinValue;

        /// <summary>
        /// Gets the span.
        /// </summary>
        public static TimeSpan Span
        {
            get
            {
                return endDate.Subtract(startDate);
            }
        }

        /// <summary>
        /// The start.
        /// </summary>
        public static void Start()
        {
            startDate = DateTime.Now;
        }

        /// <summary>
        /// The end.
        /// </summary>
        public static void End()
        {
            endDate = DateTime.Now;
        }

        /// <summary>
        /// The get seconds.
        /// </summary>
        /// <returns>
        /// The <see cref="double"/>.
        /// </returns>
        public static double GetSeconds()
        {
            return endDate == DateTime.MinValue ? 0.0 : Span.TotalSeconds;
        }
    }
}
