﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LockBitmap.cs" company="Manfred Pilhar">
//   Manfred Pilhar
// </copyright>
// <summary>
//   Defines the LockBitmap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DiffusionVisualizer
{
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Runtime.InteropServices;

    /// <summary>
    /// The lock bitmap.
    /// </summary>
    public class LockBitmap
    {
        /// <summary>
        /// The source.
        /// </summary>
        private readonly Bitmap source;

        /// <summary>
        /// The value.
        /// </summary>
        private IntPtr iptr = IntPtr.Zero;

        /// <summary>
        /// The bitmap data.
        /// </summary>
        private BitmapData bitmapData;

        /// <summary>
        /// Initializes a new instance of the <see cref="LockBitmap"/> class.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        public LockBitmap(Bitmap source)
        {
            this.source = source;
        }

        /// <summary>
        /// Gets or sets the pixels.
        /// </summary>
        public byte[] Pixels { get; set; }

        /// <summary>
        /// Gets the depth.
        /// </summary>
        public int Depth { get; private set; }

        /// <summary>
        /// Gets the width.
        /// </summary>
        public int Width { get; private set; }

        /// <summary>
        /// Gets the height.
        /// </summary>
        public int Height { get; private set; }

        /// <summary>
        /// Lock bitmap data
        /// </summary>
        public void LockBits()
        {
            // Get width and height of bitmap
            this.Width = this.source.Width;
            this.Height = this.source.Height;

            // get total locked pixels count
            var pixelCount = this.Width * this.Height;

            // Create rectangle to lock
            var rect = new Rectangle(0, 0, this.Width, this.Height);

            // get source bitmap pixel format size
            this.Depth = Image.GetPixelFormatSize(this.source.PixelFormat);

            // Check if bpp (Bits Per Pixel) is 8, 24, or 32
            if (this.Depth != 8 && this.Depth != 24 && this.Depth != 32)
            {
                throw new ArgumentException("Only 8, 24 and 32 bpp images are supported.");
            }

            // Lock bitmap and return bitmap data
            this.bitmapData = this.source.LockBits(rect, ImageLockMode.ReadWrite, this.source.PixelFormat);

            // create byte array to copy pixel values
            int step = this.Depth / 8;
            this.Pixels = new byte[pixelCount * step];
            this.iptr = this.bitmapData.Scan0;

            // Copy data from pointer to array
            Marshal.Copy(this.iptr, this.Pixels, 0, this.Pixels.Length);
        }

        /// <summary>
        /// Unlock bitmap data
        /// </summary>
        public void UnlockBits()
        {
            // Copy data from byte array to pointer
            Marshal.Copy(this.Pixels, 0, this.iptr, this.Pixels.Length);

            // Unlock bitmap data
            this.source.UnlockBits(this.bitmapData);
        }

        /// <summary>
        /// Get the color of the specified pixel
        /// </summary>
        /// <param name="x">The x</param>
        /// <param name="y">The y</param>
        /// <returns>The color</returns>
        public Color GetPixel(int x, int y)
        {
            Color clr = Color.Empty;

            // Get color components count
            int ccount = this.Depth / 8;

            // Get start index of the specified pixel
            int i = ((y * this.Width) + x) * ccount;

            if (i > this.Pixels.Length - ccount)
            {
                throw new IndexOutOfRangeException();
            }

            // For 32 bpp get Red, Green, Blue and Alpha
            if (this.Depth == 32) 
            {
                byte b = this.Pixels[i];
                byte g = this.Pixels[i + 1];
                byte r = this.Pixels[i + 2];
                byte a = this.Pixels[i + 3]; // a
                clr = Color.FromArgb(a, r, g, b);
            }

            // For 24 bpp get Red, Green and Blue
            if (this.Depth == 24) 
            {
                byte b = this.Pixels[i];
                byte g = this.Pixels[i + 1];
                byte r = this.Pixels[i + 2];
                clr = Color.FromArgb(r, g, b);
            }

            // For 8 bpp get color value (Red, Green and Blue values are the same)
            if (this.Depth == 8)
            {
                byte c = this.Pixels[i];
                clr = Color.FromArgb(c, c, c);
            }

            return clr;
        }

        /// <summary>
        /// Set the color of the specified pixel
        /// </summary>
        /// <param name="x">The x</param>
        /// <param name="y">The y</param>
        /// <param name="color">The color</param>
        public void SetPixel(int x, int y, Color color)
        {
            // Get color components count
            int ccount = this.Depth / 8;

            // Get start index of the specified pixel
            int i = ((y * this.Width) + x) * ccount;

            // For 32 bpp set Red, Green, Blue and Alpha
            if (this.Depth == 32) 
            {
                this.Pixels[i] = color.B;
                this.Pixels[i + 1] = color.G;
                this.Pixels[i + 2] = color.R;
                this.Pixels[i + 3] = color.A;
            }

            // For 24 bpp set Red, Green and Blue
            if (this.Depth == 24) 
            {
                this.Pixels[i] = color.B;
                this.Pixels[i + 1] = color.G;
                this.Pixels[i + 2] = color.R;
            }

            // For 8 bpp set color value (Red, Green and Blue values are the same)
            if (this.Depth == 8)            
            {
                this.Pixels[i] = color.B;
            }
        }
    }
}
