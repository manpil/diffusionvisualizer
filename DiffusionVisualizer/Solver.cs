﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Solver.cs" company="Manfred Pilhar">
//   Manfred Pilhar
// </copyright>
// <summary>
//   Defines the Solver type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DiffusionVisualizer
{
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;

    /// <summary>
    /// The solver.
    /// </summary>
    public class Solver
    {
        /// <summary>
        /// The image width.
        /// </summary>
        private readonly int width;

        /// <summary>
        /// The image height.
        /// </summary>
        private readonly int height;

        /// <summary>
        /// The bmp.
        /// </summary>
        private readonly Bitmap bmp;

        /// <summary>
        /// The my image codec info.
        /// </summary>
        private readonly ImageCodecInfo myImageCodecInfo;

        /// <summary>
        /// The my encoder.
        /// </summary>
        private readonly Encoder myEncoder;

        /// <summary>
        /// The conductivity.
        /// </summary>
        private readonly double diffusivity;

        /// <summary>
        /// The time steps.
        /// </summary>
        private readonly int timesteps;

        /// <summary>
        /// The next.
        /// </summary>
        private readonly double[,] next;

        /// <summary>
        /// The now.
        /// </summary>
        private double[,] now;

        /// <summary>
        /// Initializes a new instance of the <see cref="Solver"/> class.
        /// </summary>
        /// <param name="diffusivity">
        /// The diffusivity.
        /// </param>
        /// <param name="timesteps">
        /// The time steps.
        /// </param>
        /// <param name="width">
        /// The width.
        /// </param>
        /// <param name="height">
        /// The height.
        /// </param>
        public Solver(double diffusivity, int timesteps, int width, int height)
        {
            this.width = width;
            this.height = height;
            this.bmp = new Bitmap(this.width, this.height);
            this.myImageCodecInfo = this.GetEncoderInfo("image/tiff");
            this.myEncoder = Encoder.ColorDepth;
            this.diffusivity = diffusivity;
            this.timesteps = timesteps;

            this.now = new double[this.width, this.height];
            this.next = new double[this.width, this.height];

            // fill arrays with room temperature of 21 degrees celsius
            Array.Clear(this.next, 0, this.next.Length);
            Array.Clear(this.now, 0, this.now.Length);
        }

        /// <summary>
        /// The start method.
        /// </summary>
        public void Start()
        {
            // set initial condition 300 degree on upper left corner
            this.now[0, 0] = 300;
            this.next[0, 0] = 300;

            for (int i = 0; i < this.timesteps; i++)
            {
                this.Calculate();
                if (i % 100 == 0)
                {
                    this.CreateBitmap(i);
                }

                this.now = this.next;
            }   
        }

        /// <summary>
        /// The calculate method.
        /// </summary>
        public void Calculate()
        {
            for (int i = 0; i < this.height - 1; i++)
            {
                for (int j = 0; j < this.width - 1; j++)
                {
                    double tempDiffX = this.now[i, j] - this.now[i, j + 1];
                    double tempDiffY = this.now[i, j] - this.now[i + 1, j];

                    this.next[i, j + 1] = this.now[i, j + 1] + (this.diffusivity * tempDiffX);
                    this.next[i + 1, j] = this.now[i + 1, j] + (this.diffusivity * tempDiffY);
                }
            }
        }

        /// <summary>
        /// The get encoder info.
        /// </summary>
        /// <param name="mimeType">
        /// The mime type.
        /// </param>
        /// <returns>
        /// The <see cref="ImageCodecInfo"/>.
        /// </returns>
        private ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            int j;
            ImageCodecInfo[] encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                {
                    return encoders[j];
                }
            }

            return null;
        }

        /// <summary>
        /// The create bitmap.
        /// </summary>
        /// <param name="number">
        /// The number.
        /// </param>
        private void CreateBitmap(int number)
        {
            LockBitmap lockBitmap = new LockBitmap(this.bmp);
            lockBitmap.LockBits();

            Color color;
            for (int y = 0; y < lockBitmap.Height; y++)
            {
                for (int x = 0; x < lockBitmap.Width; x++)
                {
                    var hue = this.CalculateHueFromTemperature(this.now[y, x]);
                    const int Saturation = 1;                    

                    color = this.ColorFromHSV(hue, Saturation, 1);
                    lockBitmap.SetPixel(x, y, color);
                }
            }

            lockBitmap.UnlockBits();

            var myEncoderParameters = new EncoderParameters(1);

            // Save the image with a color depth of 8 bits per pixel.
            var myEncoderParameter = new EncoderParameter(this.myEncoder, 8L);
            myEncoderParameters.Param[0] = myEncoderParameter;
            var filename = "Result_" + number + ".tiff";
            this.bmp.Save(filename, this.myImageCodecInfo, myEncoderParameters);
        }

        /// <summary>
        /// The calculate hue from temperature.
        /// </summary>
        /// <param name="temperature">
        /// The temperature.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        private int CalculateHueFromTemperature(double temperature)
        {
            var onePercent = temperature / 300;
            var hue = 240 - Convert.ToInt32(onePercent * 240);
            return hue;
        }

        /// <summary>
        /// Creates color from HSV values.
        /// </summary>
        /// <param name="hue">
        /// The hue.
        /// </param>
        /// <param name="saturation">
        /// The saturation.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="Color"/>.
        /// </returns>
        private Color ColorFromHSV(double hue, double saturation, double value)
        {
            int hi = Convert.ToInt32(Math.Floor(hue / 60)) % 6;
            double f = (hue / 60) - Math.Floor(hue / 60);

            value = value * 255;
            int v = Convert.ToInt32(value);
            int p = Convert.ToInt32(value * (1 - saturation));
            int q = Convert.ToInt32(value * (1 - (f * saturation)));
            int t = Convert.ToInt32(value * (1 - ((1 - f) * saturation)));

            switch (hi)
            {
                case 0:
                    return Color.FromArgb(255, v, t, p);
                case 1:
                    return Color.FromArgb(255, q, v, p);
                case 2:
                    return Color.FromArgb(255, p, v, t);
                case 3:
                    return Color.FromArgb(255, p, q, v);
                case 4:
                    return Color.FromArgb(255, t, p, v);
                default:
                    return Color.FromArgb(255, v, p, q);
            }
        }
    }
}
