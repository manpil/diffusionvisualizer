﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Manfred Pilhar">
//   Manfred Pilhar
// </copyright>
// <summary>
//   Interaction logic for MainWindow.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DiffusionVisualizer
{
    /// <summary>
    /// Interaction logic for MainWindow
    /// </summary>
    public partial class MainWindow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();

            double diffusivity = 0.1;
            int timestamps = 1000;
            int width = 1000;
            int height = 1000;

            var solver = new Solver(diffusivity, timestamps, width, height);    
            solver.Start();        
        }
    }
}
